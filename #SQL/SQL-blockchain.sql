-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16-Nov-2018 às 13:33
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blockchain`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clausula_primeira`
--

CREATE TABLE `clausula_primeira` (
  `rua_clausula_primeira` varchar(80) DEFAULT NULL,
  `numero_clausula_primeira` int(4) DEFAULT NULL,
  `bairro_clausula_primeira` varchar(50) DEFAULT NULL,
  `cep_clausula_primeira` int(8) DEFAULT NULL,
  `cidade_clausula_primeira` varchar(20) DEFAULT NULL,
  `estado_locador` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clausula_segunda`
--

CREATE TABLE `clausula_segunda` (
  `meses_locacao` int(4) DEFAULT NULL,
  `data_inicial` int(8) DEFAULT NULL,
  `data_final` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clausula_terceira`
--

CREATE TABLE `clausula_terceira` (
  `valor` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fiador`
--

CREATE TABLE `fiador` (
  `codigo_fiador` int(4) NOT NULL,
  `nome_fiador` varchar(80) DEFAULT NULL,
  `estado_fiador` varchar(15) DEFAULT NULL,
  `profissao_fiador` varchar(40) DEFAULT NULL,
  `rg_fiador` int(8) DEFAULT NULL,
  `cpf_fiador` int(11) DEFAULT NULL,
  `rua_fiador` varchar(80) DEFAULT NULL,
  `numero_fiador` int(4) DEFAULT NULL,
  `bairro_fiador` varchar(50) DEFAULT NULL,
  `cep_fiador` int(8) DEFAULT NULL,
  `cidade_fiador` varchar(20) DEFAULT NULL,
  `hash_fiador` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `locador`
--

CREATE TABLE `locador` (
  `codigo_locador` int(4) NOT NULL,
  `nome_locador` varchar(80) DEFAULT NULL,
  `estado_locador` varchar(15) DEFAULT NULL,
  `profissao_locador` varchar(40) DEFAULT NULL,
  `rg_locador` int(8) DEFAULT NULL,
  `cpf_locador` int(11) DEFAULT NULL,
  `hash_locador` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `locatario`
--

CREATE TABLE `locatario` (
  `codigo_locatario` int(4) NOT NULL,
  `nome_locatario` varchar(80) DEFAULT NULL,
  `estado_locatario` varchar(15) DEFAULT NULL,
  `profissao_locatario` varchar(40) DEFAULT NULL,
  `rg_locatario` int(8) DEFAULT NULL,
  `cpf_locatario` int(11) DEFAULT NULL,
  `hash_locatario` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fiador`
--
ALTER TABLE `fiador`
  ADD PRIMARY KEY (`codigo_fiador`);

--
-- Indexes for table `locador`
--
ALTER TABLE `locador`
  ADD PRIMARY KEY (`codigo_locador`);

--
-- Indexes for table `locatario`
--
ALTER TABLE `locatario`
  ADD PRIMARY KEY (`codigo_locatario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fiador`
--
ALTER TABLE `fiador`
  MODIFY `codigo_fiador` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locador`
--
ALTER TABLE `locador`
  MODIFY `codigo_locador` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locatario`
--
ALTER TABLE `locatario`
  MODIFY `codigo_locatario` int(4) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
